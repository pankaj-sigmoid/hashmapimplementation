package com.company;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        MyHashMap<String,Integer> hm=new MyHashMap<String, Integer>();
        System.out.println(hm.isEmpty());
        hm.put("A",1);
        hm.put("B",2);
        hm.put("C",3);
        hm.put("D",4);
        System.out.println(hm.put("E",5));

        System.out.println(hm.size());
        System.out.println(hm.get("C"));
        System.out.println(hm.isEmpty());
        System.out.println(hm.containsKey("B"));
        System.out.println(hm.containsKey("F"));
        System.out.println(hm.containsValue(3));
        System.out.println(hm.containsValue(8));

        for(String s: hm.keySet())
            System.out.println(s+" "+hm.get(s));

        for(Integer x: hm.values())
            System.out.println(x);

        hm.remove("C");

        for(Map.Entry e: hm.entrySet())
            System.out.println(e.getKey()+" "+e.getValue());

        System.out.println(hm.remove("B"));
        System.out.println(hm.get("B"));

        hm.clear();
        System.out.println(hm.size());
    }
}
