package com.company;

import java.util.*;

public class MyHashMap<K,V> implements Map<K,V> {

    /**
     * The default initial capacity - MUST be a power of two.
     */
    static final int DEFAULT_INITIAL_CAPACITY = 16;

    /**
     * The maximum capacity, used if a higher value is implicitly specified
     * by either of the constructors with arguments.
     * MUST be a power of two <= 1<<30.
     */
    static final int MAXIMUM_CAPACITY = 1 << 30;

    /**
     * The load factor used when none specified in constructor.
     */
    static final float DEFAULT_LOAD_FACTOR = 0.75f;

    /**
     * The table, resized as necessary. Length MUST Always be a power of two.
     */
    transient Entry[] table;

    /**
     * The number of key-value mappings contained in this map.
     */
    transient int size;

    /**
     * The next size value at which to resize (capacity * load factor).
     * @serial
     */
    int threshold;

    /**
     * The load factor for the hash table.
     *
     * @serial
     */
    final float loadFactor;


    //--------------- Constructors -------------------------
    /**
     * Constructs an empty <tt>HashMap</tt> with the default initial capacity
     * (16) and the default load factor (0.75).
     */
    public MyHashMap() {
        this.loadFactor = DEFAULT_LOAD_FACTOR;
        threshold = (int)(DEFAULT_INITIAL_CAPACITY * DEFAULT_LOAD_FACTOR);
        table = new Entry[DEFAULT_INITIAL_CAPACITY];
    }

    /**
     * Constructs an empty <tt>HashMap</tt> with the specified initial
     * capacity and load factor.
     *
     * @param  initialCapacity the initial capacity
     * @param  loadFactor      the load factor
     * @throws IllegalArgumentException if the initial capacity is negative
     *         or the load factor is nonpositive
     */

    public MyHashMap(int initialCapacity, float loadFactor) {
        if (initialCapacity < 0)
            throw new IllegalArgumentException("Illegal initial capacity: "
                    + initialCapacity + ", initial capacity cannot be be negative");

        if (initialCapacity > MAXIMUM_CAPACITY)
            initialCapacity = MAXIMUM_CAPACITY;

        if (loadFactor <= 0 || Float.isNaN(loadFactor))
            throw new IllegalArgumentException("Illegal load factor: " + loadFactor);

        int capacity = 1;
        while (capacity < initialCapacity)
            capacity <<= 1;

        this.loadFactor = loadFactor;
        threshold = (int)(capacity * loadFactor);
        table = new Entry[capacity];
    }

    /**
     * Constructs an empty <tt>HashMap</tt> with the specified initial
     * capacity and the default load factor (0.75).
     *
     * @param  initialCapacity the initial capacity.
     * @throws IllegalArgumentException if the initial capacity is negative.
     */
    public MyHashMap(int initialCapacity) {
        this(initialCapacity, DEFAULT_LOAD_FACTOR);
    }

    /**
     * Constructs a new <tt>HashMap</tt> with the same mappings as the
     * specified <tt>Map</tt>.  The <tt>HashMap</tt> is created with
     * default load factor (0.75) and an initial capacity sufficient to
     * hold the mappings in the specified <tt>Map</tt>.
     *
     * @param   m the map whose mappings are to be placed in this map
     */
    public MyHashMap(Map<? extends K, ? extends V> m) {
        this(Math.max((int)(m.size() / DEFAULT_LOAD_FACTOR) + 1, DEFAULT_INITIAL_CAPACITY), DEFAULT_LOAD_FACTOR);
        for (Map.Entry<? extends K, ? extends V> e : m.entrySet())
            put(e.getKey(), e.getValue());
    }

    //-------------------- internal utilities ------------------------

    /**
     * This function ensures that hashCodes that differ only by
     * constant multiples at each bit position have a bounded
     * number of collisions (approximately 8 at default load factor).
     *
     * @param h provided hash code
     * @return corresponding hashcode.
     */
    static int hash(int h) {
        h ^= (h >>> 20) ^ (h >>> 12);
        return h ^ (h >>> 7) ^ (h >>> 4);
    }

    /**
     * Returns index for hash code h.
     *
     * @param h hash code whose index to be calculated
     * @param length length of the Entry table
     * @return index corresponding to given hash code.
     */
    static int indexFor(int h, int length) {
        return h & (length-1);
    }

    /**
     * Returns size of the HashMap.
     *
     * @return the number of key-value mappings in this map
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Returns {@code true} if HashMap is empty, false otherwise.
     *
     * @return {@code true} if no key-value mapping is present in the HashMap
     */
    @Override
    public boolean isEmpty() {
        return size==0;
    }

    /**
     * Returns {@code true} if this map contains a mapping for the given key.
     *
     * @param key The key whose presence in this map is to be tested
     * @return {@code true} if this map contains a mapping for the specified key.
     */
    @Override
    public boolean containsKey(Object key) {
        if(get(key)==null) return false;
        return true;
    }

    /**
     * Returns {@code true} if this map maps one or more keys to the specified value.
     *
     * @param value value whose presence in this map is to be tested
     * @return {@code true} if this map maps one or more keys to the specified value
     */
    @Override
    public boolean containsValue(Object value) {
        for (int i = 0; i < table.length ; i++)
            for (Entry e = table[i]; e != null ; e = e.next)
                if (value.equals(e.value))
                    return true;
        return false;
    }

    /**
     * Returns the value to which the specified key is mapped,
     * or null if this map contains no mapping for the key.
     *
     * @param key key for which mapped value is to be found
     * @return value mapped with specified key
     *      or {@code null} if no mapping for the key
     */
    @Override
    public V get(Object key) {
        int hash = (key == null) ? 0 : hash(key.hashCode());
        int index=0;
        if(key!=null) index=indexFor(hash, table.length);

        for (Entry<K,V> e = table[index]; e!=null; e=e.next)
            if (e.hash == hash && (key==e.key || key.equals(e.key)))
                return e.value;

        return null;
    }

    /**
     * Associates the specified value with the specified key in this map.
     * If the map previously contained a mapping for the key, the old
     * value is replaced. Key should be unique whereas value can be anything
     *
     * @param key specified key
     * @param value value associated with the key
     * @return old value associated with the key
     *      or {@code null} if there was no mapping for the key
     */
    @Override
    public V put(K key, V value) {
        //if (key == null)
          //  return putForNullKey(value);
        int hash = (key == null) ? 0 : hash(key.hashCode());
        int i = indexFor(hash, table.length);
        for (Entry<K,V> e = table[i]; e != null; e = e.next) {
            if (e.hash == hash && key.equals(e.key)) {
                V oldValue = e.value;
                e.value = (V)value;
                return oldValue;
            }
        }

        Entry<K,V> e = table[i];
        table[i] = new Entry(key, value, e, hash);
        if (size++ >= threshold)
            resize(2 * table.length);
        return (V)table[i].value;
    }

    /**
     * Rehashes the contents of this map into a new array with a larger capacity.
     * This method is called automatically when the number of keys in this map reaches its threshold.
     *
     * @param newCapacity new size of the table
     */
    void resize(int newCapacity) {
        if (table.length == MAXIMUM_CAPACITY) {
            threshold = Integer.MAX_VALUE;
            return;
        }

        Entry[] newTable = new Entry[newCapacity];
        for (int j = 0; j < table.length; j++) {
            Entry<K,V> e = table[j];
            if (e != null) {
                table[j] = null;
                do {
                    Entry<K,V> next = e.next;
                    int i = indexFor(e.hash, newCapacity);
                    e.next = newTable[i];
                    newTable[i] = e;
                    e = next;
                } while (e != null);
            }
        }

        table = newTable;
        threshold = (int)(newCapacity * loadFactor);
    }

    /**
     * Removes the mapping for the specified key from this map if present.
     *
     * @param key key corresponding to which mapping is to be removed
     * @return the previous value associated with key,
     *      or {@code null} if there was no mapping for key.
     */
    @Override
    public V remove(Object key) {
        int hash = (key == null) ? 0 : hash(key.hashCode());
        int i = indexFor(hash, table.length);
        Entry<K,V> prev = table[i];
        Entry<K,V> e = prev;

        while (e != null) {
            Entry<K,V> next = e.next;
            if (e.hash == hash && key.equals(e.key)) {
                size--;
                if (prev == e)
                    table[i] = next;
                else
                    prev.next = next;
                return e.value;
            }
            prev = e;
            e = next;
        }

        return e.value;
    }

    /**
     * Copies all of the mappings from the specified map to this map.
     *
     * @param m mappings to be stored in this map
     */
    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        int n = m.size();
        if (n == 0)
            return;

        if (n> threshold) {
            int targetCapacity = (int)(n/ loadFactor + 1);
            if (targetCapacity > MAXIMUM_CAPACITY)
                targetCapacity = MAXIMUM_CAPACITY;

            int newCapacity = table.length;
            while (newCapacity < targetCapacity)
                newCapacity <<= 1;

            if (newCapacity > table.length)
                resize(newCapacity);
        }

        for (Map.Entry<? extends K, ? extends V> e : m.entrySet())
            put(e.getKey(), e.getValue());
    }

    /**
     * Removes all of the mappings from this map.
     * The map will be empty after this call returns.
     */
    @Override
    public void clear() {
        int n=table.length;
        for (int i = 0; i < n; i++)
            table[i] = null;
        size = 0;
    }

    //--------------------- Views -------------------------------

    private transient Set<K> keySet = null;
    private transient Collection<V> values = null;
    private transient Set<Map.Entry<K,V>> entrySet = null;

    /**
     * Returns a Set view of the keys contained in this map.
     *
     * @return a set view of the keys contained in this map
     */
    @Override
    public Set<K> keySet() {
        Set<K> ks = keySet;
        return (ks != null ? ks : (keySet = new KeySet()));
    }

    private final class KeySet extends AbstractSet<K> {

        @Override
        public Iterator<K> iterator() {
            return new KeyIterator();
        }

        @Override
        public int size() {
            return size;
        }

        @Override
        public boolean contains(Object o) {
            return containsKey(o);
        }

        @Override
        public boolean remove(Object o) {
            return  remove(o);
        }

        @Override
        public void clear() {
            this.clear();
        }
    }

    /**
     * Returns a Collection view of the values contained in this map.
     *
     * @return a view of the values contained in this map
     */
    @Override
    public Collection<V> values() {
        Collection<V> vs = values;
        return (vs != null ? vs : (values = new Values()));
    }

    private final class Values extends AbstractCollection<V> {
        @Override
        public Iterator<V> iterator() {
            return new ValueIterator();
        }

        @Override
        public int size() {
            return size;
        }

        @Override
        public boolean contains(Object o) {
            return containsValue(o);
        }

        @Override
        public void clear() {
            MyHashMap.this.clear();
        }
    }

    /**
     * Returns a set view of the mappings contained in this map.
     * The set is backed by the map, so changes to the map are
     * reflected in the set, and vice-versa.
     *
     * @return a set view of the mappings contained in this map
     */
    @Override
    public Set<Map.Entry<K, V>> entrySet() {
        Set<Map.Entry<K,V>> es = entrySet;
        return es != null ? es : (entrySet = new EntrySet());
    }

    private final class EntrySet extends AbstractSet<Map.Entry<K,V>> {
        @Override
        public Iterator<Map.Entry<K,V>> iterator() {
            return new EntryIterator();
        }

        @Override
        public boolean contains(Object o) {
            if (!(o instanceof Map.Entry))
                return false;

            Map.Entry<K,V> obj = (Map.Entry<K,V>) o;
            K key=obj.getKey();
            V val=obj.getValue();

            int hash = (key == null) ? 0 : hash(key.hashCode());
            int i=indexFor(hash, table.length);
            for (Entry<K,V> e = table[i]; e != null; e = e.next) {
                if (e.hash == hash && (e.key == key || (key != null && key.equals(e.key))))
                    if(e.value==val) return true;
            }

            return false;
        }

        @Override
        public boolean remove(Object o) {
            return MyHashMap.this.remove(o) != null;
        }

        @Override
        public int size() {
            return size;
        }

        @Override
        public void clear() {
            MyHashMap.this.clear();
        }
    }

    //----------------------- Iterators -----------------

    private abstract class MyHashIterator<E> implements Iterator<E>{
        Entry<K,V> next;        // next entry to return
        int index;              // current slot
        Entry<K,V> current;     // current entry

        MyHashIterator() {
            if (size > 0) { // advance to first entry
                Entry[] t = table;
                while (index < t.length && (next = t[index++]) == null) ;
            }
        }

        public final boolean hasNext() {
            return next != null;
        }

        final Entry<K,V> nextEntry() {
            Entry<K,V> e = next;
            if (e == null)
                throw new NoSuchElementException();

            if ((next = e.next) == null) {
                Entry[] t = table;
                while (index < t.length && (next = t[index++]) == null)
                    ;
            }
            current = e;
            return e;
        }

        public void remove() {
            if (current == null)
                throw new IllegalStateException();
            Object k = current.key;
            current = null;
            MyHashMap.this.remove(k);
        }
    }

    private final class ValueIterator extends MyHashIterator<V> {
        public V next() {
            return nextEntry().value;
        }
    }

    private final class KeyIterator extends MyHashIterator<K> {
        public K next() {
            return nextEntry().getKey();
        }
    }

    private final class EntryIterator extends MyHashIterator<Map.Entry<K,V>> {
        public Map.Entry<K,V> next() {
            return nextEntry();
        }
    }

    //--------------------- Entry class ----------------------

    static class Entry<K,V> implements Map.Entry<K,V> {
        final K key;
        V value;
        Entry<K,V> next;
        final int hash;

        public Entry(K key, V value, Entry<K, V> next, int hash) {
            this.key = key;
            this.value = value;
            this.next = next;
            this.hash = hash;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            V old = this.value;
            this.value = value;
            return old;
        }

        public final boolean equals(Object o) {
            if (!(o instanceof Map.Entry))
                return false;
            Map.Entry e = (Map.Entry)o;
            Object k1 = getKey();
            Object k2 = e.getKey();
            if (k1 == k2 || (k1 != null && k1.equals(k2))) {
                Object v1 = getValue();
                Object v2 = e.getValue();
                if (v1 == v2 || (v1 != null && v1.equals(v2)))
                    return true;
            }
            return false;
        }

        public final int hashCode() {
            return (key==null   ? 0 : key.hashCode()) ^
                    (value==null ? 0 : value.hashCode());
        }

        public final String toString() {
            return getKey() + "=" + getValue();
        }
    }


}
