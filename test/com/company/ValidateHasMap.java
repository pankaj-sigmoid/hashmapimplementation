package com.company;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class ValidateHasMap {
    /**
     * testing isEmpty method returning true when is created and
     * also when clear method is called
     */
    @Test
    public void testEmptyMap(){
        MyHashMap<String,Integer> hm=new MyHashMap<String, Integer>();
        assertTrue(hm.isEmpty());
        hm.put("F",6);
        assertFalse(hm.isEmpty());
        hm.clear();
        assertTrue(hm.isEmpty());
    }

    /**
     * testing size method returning correct size of the map
     */
    @Test
    public void testSize(){
        MyHashMap<String,Integer> hm=new MyHashMap<String, Integer>();
        hm.put("A",1);
        hm.put("B",2);
        hm.put("C",3);
        assertEquals(hm.size(),3);
    }

    /**
     * testing put method working properly
     */
    @Test
    public void testPut(){
        MyHashMap<String,Integer> hm=new MyHashMap<String, Integer>();
        assertEquals((long)hm.put("B",2),2);
    }

    /**
     * testing get method returning correct mapped value corresponding to provided key
     */
    @Test
    public void testGet(){
        MyHashMap<String,Integer> hm=new MyHashMap<String, Integer>();
        hm.put("A",1);
        hm.put("B",2);
        hm.put("C",3);
        assertEquals((long)hm.get("B"),2);
    }

    /**
     * testing map contains specified key
     */
    @Test
    public void testContainsKey(){
        MyHashMap<String,Integer> hm=new MyHashMap<String, Integer>();
        hm.put("A",1);
        hm.put("B",2);
        hm.put("C",3);
        assertTrue(hm.containsKey("C"));
        assertFalse(hm.containsKey("X"));
    }

    /**
     * testing map contains specified value
     */
    @Test
    public void testContainsValue(){
        MyHashMap<String,Integer> hm=new MyHashMap<String, Integer>();
        hm.put("A",1);
        hm.put("B",2);
        hm.put("C",3);
        assertTrue(hm.containsValue(2));
        assertFalse(hm.containsValue(6));
    }

    /**
     * testing duplicate key overwrites the value corresponding to it
     */
    @Test
    public void testOverwritingOnDuplicateKeys(){
        MyHashMap<String,Integer> hm=new MyHashMap<String, Integer>();
        hm.put("A",1);
        hm.put("B",2);
        hm.put("C",3);
        assertEquals((long)hm.get("B"),2);
        hm.put("B",5);
        assertEquals((long)hm.get("B"),5);
    }

    /**
     * testing for null key
     */
    @Test
    public void testNullKey(){
        MyHashMap<String,Integer> hm=new MyHashMap<String, Integer>();
        hm.put("A",1);
        hm.put(null,6);
        hm.put("B",2);
        hm.put("C",3);
        assertEquals((long)hm.get(null),6);
    }

    /**
     * testing for null value
     */
    @Test
    public void testNullValue(){
        MyHashMap<String,Integer> hm=new MyHashMap<String, Integer>();
        hm.put("A",1);
        hm.put("B",2);
        hm.put("D",null);
        hm.put("C",3);
        assertNull(hm.get("D"));
    }

    /**
     * testing any mapping is removed using remove method by passing key to it
     */
    @Test
    public void testRemove(){
        MyHashMap<String,Integer> hm=new MyHashMap<String, Integer>();
        hm.put("A",1);
        hm.put("B",2);
        hm.put("C",3);
        assertEquals((long)hm.remove("B"),2);
        assertNull(hm.get("B"));
    }

    /**
     * testing clear method removes all entries from the map
     */
    @Test
    public void testClear(){
        MyHashMap<String,Integer> hm=new MyHashMap<String, Integer>();
        hm.put("S",8);
        hm.put("F",6);
        assertFalse(hm.isEmpty());
        hm.clear();
        assertTrue(hm.isEmpty());
    }

    /**
     * testing putAll method by combining two HashMaps
     */
    @Test
    public void testPutAll(){
        MyHashMap<String,Integer> hm=new MyHashMap<String, Integer>();
        hm.put("S",8);
        hm.put("F",6);

        MyHashMap<String,Integer> hm1=new MyHashMap<String, Integer>();
        hm1.put("A",1);
        hm1.put("B",2);

        hm.putAll(hm1);
        assertEquals(hm.size(),4);
        assertEquals((long)hm.get("A"),1);
    }

    @Test
    public void testKeySet(){
        MyHashMap<String,Integer> hm=new MyHashMap<String, Integer>();
        hm.put("A",1);
        hm.put("B",2);
        hm.put("C",3);
        Set<String> keys=new HashSet<String>();
        keys.add("A");
        keys.add("B");
        keys.add("C");
        assertEquals(hm.keySet(),keys);
    }

}
